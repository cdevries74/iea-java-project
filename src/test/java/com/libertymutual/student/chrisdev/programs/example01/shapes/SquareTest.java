package com.libertymutual.student.chrisdev.programs.example01.shapes;

import org.junit.Test;

// import sun.security.util.Length;

import java.awt.Color;
import java.beans.Transient;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class SquareTest {

    @Test
    public void testShape() {
        Square square = new Square(100, Color.RED);
        BigDecimal area = square.getArea();
        BigDecimal expectedAnswer = new BigDecimal(10000);
        assertEquals("Shape is not square", expectedAnswer, area);
    }

}