package com.libertymutual.student.chrisdev.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;

public class Rectangle extends Shape {

    private int width;
    private int height;

    public Rectangle(int width, int height, Color color) {
        super(color);
        this.width = width;
        this.height = height;
    }

    // provide a getArea implementation 
    @Override
    public BigDecimal getArea() {
        double area = width * height;
        return new BigDecimal(area);
    }
}