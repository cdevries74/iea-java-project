package com.libertymutual.student.chrisdev.programs.example01.shapes;

import java.awt.Color;
import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Square extends Shape {

    private final static Logger logger = LogManager.getLogger(Square.class);
    
    private int side;

    public Square(int side, Color color) {
        super(color);
        this.side = side;
    }

    // provide a getArea implementation 
    @Override
    public BigDecimal getArea() {
        
        logger.info(Square.class + " gets the X ");

        double area = side * side;
        return new BigDecimal(area);
    }
}