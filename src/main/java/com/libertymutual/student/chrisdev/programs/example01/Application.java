package com.libertymutual.student.chrisdev.programs.example01;

import java.awt.Color;
import java.math.BigDecimal;
import com.libertymutual.student.chrisdev.programs.example01.shapes.Shape;
import com.libertymutual.student.chrisdev.programs.example01.shapes.Circle;
import com.libertymutual.student.chrisdev.programs.example01.shapes.Square;
import com.libertymutual.student.chrisdev.programs.example01.shapes.Rectangle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {

    public final static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

        logger.info(Application.class + " HELLO WORLD starting ");
        
        int radius = 10;
        Circle circle = new Circle(radius, Color.PINK);
        System.out.println(circle.getArea());

        int length = 100;
        Square square = new Square(length, Color.RED);
        System.out.println(square.getArea());

        int width = 100;
        int height = 10;
        Rectangle rectangle = new Rectangle(width, height, Color.BLUE);
        System.out.println(rectangle.getArea());

        System.exit(0);
    }
}